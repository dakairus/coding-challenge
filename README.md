## Description

This is a coding challenge public repository for a basic example of Nest JS + Typescript + graphql + TypeOrm + mysql project

## Installation

```bash
$ yarn install
```

## Requirements

This App requires an Mysql installation. Modify the .env file to specify the connection paramenters

## Running the app

```bash
# watch mode
$ yarn run start:dev
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

import { forwardRef, Module } from '@nestjs/common';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';
import { NftModule } from '../nft/nft.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModel } from './user.model';

@Module({
  imports: [forwardRef(() => NftModule), TypeOrmModule.forFeature([UserModel])],
  providers: [UserResolver, UserService],
  exports: [UserService],
})
export class UserModule {}

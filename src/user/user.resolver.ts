import * as jwt from 'jsonwebtoken';
import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { UserModel } from './user.model';
import { UserService } from './user.service';
import { Inject, UseGuards } from '@nestjs/common';
import { NftService } from '../nft/nft.service';
import { ConfigService } from '@nestjs/config';
import { NftModel } from '../nft/nft.model';
import { AuthorizationGuard } from '../authorization/authorization.guard';

@Resolver((of) => UserModel)
export class UserResolver {
  constructor(
    @Inject(UserService) private userService: UserService,
    @Inject(NftService) private nftService: NftService,
    @Inject(ConfigService) private configService: ConfigService,
  ) {}

  @Mutation((returns) => String)
  async login(
    @Args('email') email: string,
    @Args('password') password: string,
  ) {
    const user = await this.userService.findByLogin(email, password);
    return jwt.sign(
      { id: user.id },
      this.configService.get<string>('JWT_SECRET'),
    );
  }

  @ResolveField((returns) => [NftModel])
  @UseGuards(AuthorizationGuard)
  async nfts(@Parent() user): Promise<NftModel[]> {
    return (await this.userService.findOne(user.id, { nfts: true })).nfts;
  }

  @Mutation((returns) => UserModel)
  async createUser(
    @Args('email') email: string,
    @Args('name') name: string,
    @Args('password') password: string,
  ): Promise<UserModel> {
    return await this.userService.create({ name, email, password });
  }

  @Query((returns) => UserModel)
  @UseGuards(AuthorizationGuard)
  async user(@Args('id') id: number): Promise<UserModel> {
    return await this.userService.findOne(id);
  }
}

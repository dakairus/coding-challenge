import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { ObjectType, Field } from '@nestjs/graphql';
import { NftModel } from '../nft/nft.model';

@ObjectType()
@Entity()
export class UserModel {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;
  @Field()
  @Column({ length: 500, nullable: false })
  name: string;
  @Field()
  @Column('varchar', { nullable: false })
  email: string;
  @Field()
  @Column('varchar', { nullable: false })
  password: string;
  @Field((type) => [NftModel], { nullable: true })
  @OneToMany((type) => NftModel, (nft) => nft.user)
  nfts: NftModel[];
  @Field()
  @Column()
  @CreateDateColumn()
  created_at: Date;
  @Field()
  @Column()
  @UpdateDateColumn()
  updated_at: Date;
}

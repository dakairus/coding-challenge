import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserModel } from './user.model';
import { Repository } from 'typeorm';
import { userDTO } from './user.dto';
import { NftModel } from '../nft/nft.model';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserModel) private userRepository: Repository<UserModel>,
  ) {}
  async create(details: userDTO): Promise<UserModel> {
    console.log(details);
    return await this.userRepository.save(details);
  }

  save(user: UserModel): Promise<UserModel> {
    return this.userRepository.save(user);
  }

  findOne(id: number, relations?: any): Promise<UserModel> {
    return this.userRepository.findOne({ where: { id }, relations });
  }

  findByLogin(email: string, password: string): Promise<UserModel> {
    return this.userRepository.findOne({ where: { email, password } });
  }

  assignNFT(nftId: number, userId: number) {
    return this.userRepository
      .createQueryBuilder()
      .relation(UserModel, 'nfts')
      .of(userId)
      .add(nftId);
  }
}

export interface nftDTO {
  name: string;
  blockchainLink: string;
  description?: string;
  imageUrl?: string;
  mintDate?: Date;
}

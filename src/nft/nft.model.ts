import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { ObjectType, Field } from '@nestjs/graphql';
import { UserModel } from '../user/user.model';

@ObjectType()
@Entity()
export class NftModel {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;
  @Field()
  @Column({ length: 500, nullable: false })
  name: string;
  @Field()
  @Column('text', { nullable: false })
  blockchainLink: string;
  @Field()
  @Column('text')
  description: string;
  @Field()
  @Column('text')
  imageUrl: string;
  @Field()
  @Column('date')
  mintDate: Date;
  @Field((type) => UserModel)
  @ManyToOne((type) => UserModel, (user) => user.nfts)
  user: UserModel;
}

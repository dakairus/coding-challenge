import { forwardRef, Module } from '@nestjs/common';
import { NftService } from './nft.service';
import { NftResolver } from './nft.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from '../user/user.module';
import { NftModel } from './nft.model';

@Module({
  imports: [TypeOrmModule.forFeature([NftModel]), forwardRef(() => UserModule)],
  providers: [NftService, NftResolver],
  exports: [NftService],
})
export class NftModule {}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NftModel } from './nft.model';
import { Repository, UpdateResult } from 'typeorm';
import { UserModel } from '../user/user.model';

@Injectable()
export class NftService {
  constructor(
    @InjectRepository(NftModel) private nftRepository: Repository<NftModel>,
  ) {}
  create(details: any): Promise<NftModel> {
    return this.nftRepository.save(details);
  }

  save(nft: NftModel): Promise<NftModel> {
    return this.nftRepository.save(nft);
  }

  assignUser(nftId: number, userId: number): Promise<void> {
    return this.nftRepository
      .createQueryBuilder()
      .relation(NftModel, 'user')
      .of(nftId)
      .set(userId);
  }

  update(id: number, info: any): Promise<UpdateResult> {
    return this.nftRepository.update(id, info);
  }

  findOne(id: number, relations?: any): Promise<NftModel> {
    return this.nftRepository.findOne({ where: { id }, relations });
  }

  findByUser(userId: number): Promise<NftModel[]> {
    return this.nftRepository
      .createQueryBuilder()
      .relation(UserModel, 'nfts')
      .of(userId)
      .loadMany();
  }
}

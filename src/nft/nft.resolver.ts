import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { NftModel } from './nft.model';
import { HttpException, HttpStatus, Inject, UseGuards } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { NftService } from './nft.service';
import { AuthorizationGuard } from '../authorization/authorization.guard';

@Resolver((of) => NftModel)
export class NftResolver {
  constructor(
    @Inject(UserService) private userService: UserService,
    @Inject(NftService) private nftService: NftService,
  ) {}

  @Query((returns) => [NftModel])
  @UseGuards(AuthorizationGuard)
  async getNFTSByUser(@Context('user') user: any): Promise<NftModel[]> {
    return await this.nftService.findByUser(user.id);
  }

  @Mutation((returns) => NftModel)
  @UseGuards(AuthorizationGuard)
  async createNft(
    @Context('user') user: any,
    @Args('blockchainLink') blockchainLink: string,
    @Args('name') name: string,
    @Args('description') description: string,
    @Args('imageUrl') imageUrl: string,
    @Args('mintDate') mintDate: Date,
  ): Promise<NftModel> {
    const newNFT = await this.nftService.create({
      name,
      blockchainLink,
      description,
      imageUrl,
      mintDate,
    });
    await this.nftService.assignUser(newNFT.id, user.id);
    return await this.nftService.findOne(newNFT.id, { user: true });
  }

  @Mutation((returns) => NftModel)
  @UseGuards(AuthorizationGuard)
  async transferNFT(
    @Context() context: any,
    @Args('nftId') nftId: number,
    @Args('targetUserId') targetUserId: number,
  ): Promise<NftModel> {
    const nft = await this.nftService.findOne(nftId, { user: true });
    if (nft.user.id !== context.user.id)
      throw new HttpException(
        'Not allowed to transfer NFT that you dont own',
        HttpStatus.FORBIDDEN,
      );
    await this.userService.assignNFT(nftId, targetUserId);
    return await this.nftService.findOne(nftId, { user: true });
  }
}
